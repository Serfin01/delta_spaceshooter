﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAbebe : MonoBehaviour

{


    public float speed;

    private float timeCounter;

    private Transform target;





    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    void Update()

    {
        if (Vector2.Distance(transform.position, target.position) > 0.1)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

    }

    public void Dying()
    {
        Destroy(gameObject);
    }
}