﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay(){
        Debug.LogError("He pulsado Play");

        SceneManager.LoadScene("Game");
    }

    public void PulsaStoryBoard()
    {
        Debug.LogError("He pulsado StoryBoard");

        SceneManager.LoadScene("StoryBoard");
    }

    public void PulsaMenu()
    {
        Debug.LogError("He pulsado Menu");
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("MainMenu");
    }

    public void PulsaCreditos()
    {
        Debug.LogError("He pulsado Creditos");

        SceneManager.LoadScene("Creditos");
    }

    public void PulsaExit(){
        Application.Quit();
    }
}
