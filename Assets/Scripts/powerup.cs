﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerup : MonoBehaviour
{
    [SerializeField] GameObject[] sprites;
    [SerializeField] AudioSource audioSource;
    private int elegido;
    private float speedx = 3.0f;


    private void Awake()
    {
        elegido = Random.Range(0, sprites.Length);

        sprites[elegido].SetActive(true);
    }
    // Start is called before the first frame update
    void Start()
    {
        transform.Translate(-speedx * Time.deltaTime, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-speedx * Time.deltaTime, 0, 0);
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine(Danone());
        }
        else if (other.tag == "Finish")
        {
            Destroy(this.gameObject);
        }
    }
    IEnumerator Danone()
    {
        //Desactivo el grafico
        sprites[elegido].SetActive(false);

        //Elimino el BoxCollider2D
        GetComponent<Collider>().enabled = false;

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        Destroy(this.gameObject);
    }
}
