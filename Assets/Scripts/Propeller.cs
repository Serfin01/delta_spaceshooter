﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Propeller : MonoBehaviour {

    public TrailRenderer blue01;


    // Use this for initialization
    void Awake () {
        Stop ();
    }
    
    public void BlueFire(){
        blue01.emitting = true;
    }

    public void RedFire(){
        blue01.emitting = true;
    }

    public void Stop(){
        blue01.emitting = true;
    }
}

