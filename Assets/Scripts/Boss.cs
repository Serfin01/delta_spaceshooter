﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Boss : MonoBehaviour {

    public int health;
    public int damage;
    private float timeBtwDamage = 1.5f;

    [SerializeField] BoxCollider2D collider;
    //[SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
    private ScoreManager sm;
    public int puntuacion = 100;
    [SerializeField] GameObject[] sprites;
    private int elegido;
    public CanvasGroup slider;


    public Slider healthBar;
    //private Animator anim;
    public bool isDead;

    private void Awake()
    {
        elegido = Random.Range(0, sprites.Length);

        for (int kk = 0; kk < sprites.Length; kk++)
        {
            sprites[kk].SetActive(false);
        }

        sprites[elegido].SetActive(true);
        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();
    }

    private void Start()
    {
        //anim = GetComponent<Animator>();
        //var canvGroup = GetComponent<CanvasGroup>();
        slider.alpha = 1f;
        slider.blocksRaycasts = true;
        
    }

    private void Update()
    {

        if (health <= 25) {
            //anim.SetTrigger("stageTwo");
        }

        if (health <= 0) {

            //anim.SetTrigger("death");
        }

        // give the player some time to recover before taking more damage !
        if (timeBtwDamage > 0) {
            timeBtwDamage -= Time.deltaTime;
        }

        healthBar.value = health;

        /*if (sprites[elegido].SetActive == false)
        {
            slider.alpha = 0f; //this makes everything transparent
            slider.blocksRaycasts = false; //this prevents the UI element to receive input events
        }
        */


    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet" && health == 0)
        {
            StartCoroutine(DestroyShip());
        }
        else if (health >= 1)
        {
            health = health - 1;
        }
        else if (other.tag == "Finish")
        {
            Destroy(this.gameObject);
        }
    }


    IEnumerator DestroyShip()
    {
        //Sumar puntos
        sm.AddScore(puntuacion);

        //Desactivo el grafico
        sprites[elegido].SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        //ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        Destroy(this.gameObject);
    }
}
