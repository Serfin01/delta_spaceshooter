﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager : MonoBehaviour
{
    public GameObject[] formaciones;
    public float timeLaunchFormation;
    public float tiempo = 0.0f;
    public GameObject boss;

    private float currentTime = 0;

    void Awake()
    {
        StartCoroutine(LanzaFormacion());
    }

    public void Update()
    {
        tiempo += Time.deltaTime;
        if (tiempo == 5.0f)
        {
            Destroy(this.gameObject);
            //StartCoroutine(FinalBattle());
        }
    }

    IEnumerator LanzaFormacion()
    {
        int formacionActual = 0;
        for (int i = 0; i < 5; i++)
        {
            formacionActual = Random.Range(0, formaciones.Length);

            Instantiate(formaciones[formacionActual], new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
            yield return new WaitForSeconds(timeLaunchFormation);
        }
        Instantiate(boss, new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
    }
    IEnumerator FinalBattle()
    {
        Instantiate(boss, new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);

        yield return new WaitForSeconds(1.0f);

        Destroy(this.gameObject);
    }

}