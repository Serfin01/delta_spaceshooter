﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StoryBoard : MonoBehaviour
{
    public Sprite[] imagenes;
    public float tiempoEspera = 1f;
    public int NumImagenActual = 0;
    public GameObject visor;

    // Start is called before the first frame update
    void Start()
    {
        LoadImage();
    }

    // Update is called once per frame
    void LoadImage()
    {
        visor.GetComponent<SpriteRenderer>().sprite = imagenes[NumImagenActual];

        NumImagenActual += 1;
        Invoke("LoadImage", tiempoEspera);

        if (NumImagenActual == 5)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}