﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class xfaMuevete : MonoBehaviour
{
    public float velocity;

    void Update()
    {
        transform.Translate(velocity * Time.deltaTime, 0, 0);
    }
}
