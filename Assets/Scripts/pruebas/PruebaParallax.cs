﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PruebaParallax : MonoBehaviour
{
    public float scrollSpeed;
    public float m_BackgroundSize;

    // Update is called once per frame
    void Update()
    {
        RepeateBackground();
        if (transform.position.x < -m_BackgroundSize){
            transform.Translate(m_BackgroundSize,0,0);
        }
    }
    void RepeateBackground()
    {
        transform.Translate(-scrollSpeed*Time.deltaTime,0,0);
    }
}
