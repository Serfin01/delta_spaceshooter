﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class slider : MonoBehaviour
{
    
    private void Awake()
    {
        var canvGroup = GetComponent<CanvasGroup>();
        canvGroup.alpha = 0f; //this makes everything transparent
        canvGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }
    void Hide()
    {
        var canvGroup = GetComponent<CanvasGroup>();
        canvGroup.alpha = 0f; //this makes everything transparent
        canvGroup.blocksRaycasts = false; //this prevents the UI element to receive input events
    }

}
